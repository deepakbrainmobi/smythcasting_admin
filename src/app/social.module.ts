import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
} from "angular5-social-login";


// Configs
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("163525781041984")
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("Your-Google-Client-Id")
        },
      ]
  );
  return config;
}

@NgModule({
  declarations: [

 ],
  imports: [
    SocialLoginModule
  ],
  providers: [

    {
     provide: AuthServiceConfig,
     useFactory: getAuthServiceConfigs
    }
  ],
})
export class SocialModule { }
