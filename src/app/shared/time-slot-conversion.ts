export class TimeSlotConversion{

  public static changeMtsToTime(slot: any): any{
    return this.pad(Math.floor(slot/ 60), 2) + ':' + this.pad((slot % 60), 2);
  }

  public static pad(num:number, size:number): string {
    let s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

}
