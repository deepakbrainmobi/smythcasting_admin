export class DateFunctions{

  public static ngbTodayDate(): any{
    let currDate = new Date();
    let currDateObj = {year: currDate.getFullYear(), month: currDate.getMonth()+ 1,  day:  currDate.getDate()};
    return currDateObj;
  }

  public static YYYMMDDtoDate(value): any{
    try{
      console.log("custom pipe");
      value=value.toString();
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      let convDate: any = new Date(value.replace(pattern, '$1-$2-$3'));
      if (convDate == 'Invalid Date') {
        return ''
      }
      return convDate;
    }catch(ex){
      return '';
    }
  }

  public static ngbDateFetch(val: any): any{
    try{
      let date = new Date(val);
      let currDateObj = {year: date.getFullYear(), month: date.getMonth()+ 1,  day:  date.getDate()};
      return currDateObj;
    }catch(ex){
      return "";
    }
  }

  public static subtractNgbYear(ngDate: any, yearDiff: any): any{
    try{
      let customDate = ngDate;
      if(yearDiff)
        customDate.year = customDate.year - yearDiff;
      return customDate;
    }catch(ex){
      return '';
    }
  }


}
