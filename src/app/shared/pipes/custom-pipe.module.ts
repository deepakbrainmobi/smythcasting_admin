import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FilterNamesPipe } from './filter-names.pipe';



@NgModule({

  imports: [
    CommonModule,
  ],
  declarations:[FilterNamesPipe],
  exports: [
    FilterNamesPipe
  ]
})



export class CustomPipeModule { }
