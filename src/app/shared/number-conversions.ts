export class NumberConversions{

  public static paddingZero(val: any): any{
    var formattedNumber = ("0" + val).slice(-2);
    return formattedNumber;
  }

  public static numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

}
