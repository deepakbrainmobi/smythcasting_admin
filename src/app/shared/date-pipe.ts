import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'datePipe'})
export class CustomDatePipe implements PipeTransform {
  transform(value: string, args: string[]): any {
    if (!value) return value;
    try{
      console.log("custom pipe");
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      let convDate: any = new Date(value.replace(pattern, '$1-$2-$3'));
      if (convDate == 'Invalid Date') {
        return ''
      }
      return convDate;
    }catch(ex){
      return '';
    }
    /* date = new Date(st.replace(pattern, '$1-$2-$3'));
    */

    // $scope.date = new Date(st.replace(pattern, '$1-$2-$3'));
    // return value.replace(pattern, '$1-$2-$3', function(txt) {
    //     return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    // });
  }
}
