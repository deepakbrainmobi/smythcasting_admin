import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
// import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class TokenInterceptor implements HttpInterceptor {
  constructor(
    // public auth: AuthService,
    private cookieService: CookieService) {
    console.log('dfssssssssssssssssss');
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      setHeaders: {
        "accessToken": localStorage.getItem('adminSession') || '',
        "platform": "3",
        "Authorization":  "Basic ZGVtb19hZG1pbjphZG1pbkBkZW1v"
        // "Authorization": "Bearer " + this.cookieService.get('Session')
      }
    });
    return next.handle(request);
  }
}
