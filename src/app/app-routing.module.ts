import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [

{ path: '', loadChildren: './admin/admin.module#AdminModule' },
{ path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
{ path: '**', redirectTo: '' }



];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
