import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {map, catchError} from "rxjs/operators";
import {MainAppSettings} from './main-app-settings';

import {environment} from '../../environments/environment';


@Injectable()
export class WorkTaxService {



    constructor(private http:HttpClient) {

    }


    countryListing(){
      console.log(`${environment.API_ENDPOINT}${MainAppSettings.COUNTRY_LISTING}`);
      return this.http.get(
          `${environment.API_ENDPOINT}${MainAppSettings.COUNTRY_LISTING}`
        );
    }


    cityListing(countryId?: string){
      let apiUrl = `${MainAppSettings.CITY_LISTING}`;
      if(countryId)
        apiUrl +=countryId
      console.log(`${environment.API_ENDPOINT}${apiUrl}`);
      return this.http.get(
          `${environment.API_ENDPOINT}${apiUrl}`
        );
    }

    provinceListing(countryId?: string){
      let apiUrl = `${MainAppSettings.PROVINCE_LISTING}`;
      if(countryId)
        apiUrl +=countryId;
      console.log(`${environment.API_ENDPOINT}${apiUrl}`);
      return this.http.get(
          `${environment.API_ENDPOINT}${apiUrl}`
        );
    }



}
