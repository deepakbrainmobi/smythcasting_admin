export class MainAppSettings {

   public static COUNTRY_LISTING: string = 'extras/api/country/listing';

   public static CITY_LISTING: string = 'extras/api/city/listing/' //if counrty id append, then listing of cities w.r.t country

   public static PROVINCE_LISTING: string = 'extras/api/province/listing/'; //if counrty id append, then listing of provinces w.r.t country

}
