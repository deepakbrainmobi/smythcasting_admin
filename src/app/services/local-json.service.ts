import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {map, catchError} from "rxjs/operators";


@Injectable()
export class LocalJsonService {



    constructor(private http:HttpClient) {

    }


      getCountryCode(){
          // return this.http.get("./assets/countries.json")
          return this.http.get("./assets/CountryCodes.json")
      }



}
