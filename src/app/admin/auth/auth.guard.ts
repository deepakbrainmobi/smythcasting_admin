import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { LocalAuthService } from './local-auth.service';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private authService: LocalAuthService,
    private router: Router,
    private cookieService: CookieService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authService.isLoggedIn
      .take(1)
      .map((isLoggedIn: boolean) => {
        // console.log(isLoggedIn, "!localStorage.getItem('Session')", localStorage.getItem('Session'));
        // if(localStorage.getItem('Session')){
        //   console.log(localStorage.getItem('Session')+'in session')
        // }else{
        //   console.log(localStorage.getItem('Session')+'Not in session')
        // }
        // if (!isLoggedIn && (!localStorage.getItem('Session') || !localStorage.getItem('home'))){
        if (!isLoggedIn && (!localStorage.getItem('adminSession'))){
          this.router.navigate(['/admin/signin']);
          return false;
        }
        return true;
        // console.log(localStorage.getItem('Session')+'dd')
        // console.log(isLoggedIn);
        // if(localStorage.getItem('Session')){
        //   console.log(isLoggedIn+'I am changing to true');
        //   if (!isLoggedIn){
        //     this.authService.setLogTrue(true);
        //   }else{
        //     return true;
        //   }
        // }else{
        //   this.router.navigate(['/login']);
        //   return false;
        // }
        // return true;
      });
  }
}
