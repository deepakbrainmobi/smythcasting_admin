import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from './user';

import { HttpClient } from '@angular/common/http';
import {AppSettings} from '../services/app-settings';
import { CookieService } from 'ngx-cookie-service';

import { AlertService } from '../services/alert.service';

import {environment} from '../../../environments/environment';

@Injectable()
export class LocalAuthService {
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }


  setLogTrue(data:any) {
    this.loggedIn.next(data);
  }



  constructor(
    private router: Router,
    public http: HttpClient,
    private cookieService: CookieService,
    private alertService: AlertService
  ) {}



  login(user: User){
    if (user.identity !== '' && user.key != '' ){
      user.device_token = "fgfd";
      console.log(`${environment.API_ENDPOINT}${AppSettings.LOGIN}`);
      return this.http.post(
          `${environment.API_ENDPOINT}${AppSettings.LOGIN}`,user
        );
      // this.http.post(`${environment.API_ENDPOINT}${AppSettings.LOGIN}`,user)
      // .subscribe(
      //   (data:any) => {
      //     if (data["data"] ) {
      //        if(data["data"]["adminInfo"]){
      //          // console.log('adminInfo');
      //          this.loggedIn.next(true);
      //          return true;
      //        }else{
      //          console.log('no adminInfo');
      //          return data["data"]["messages"];
      //        }
      //      }else{
      //
      //       console.log('else');
      //      }
      //     // console.log(data);
      //     // this.loggedIn.next(true);
      //     // return data;
      //   },
      //   err => {
      //     this.loggedIn.next(false);
      //   }
      // );

      // this.loggedIn.next(true);
      // // this.router.navigate(['/']);
      // this.router.navigate(['/table']);
    }
  }

  logout(){
    let sessionInfo = this.cookieService.get('Session');
    console.log(sessionInfo);
    if(sessionInfo && sessionInfo!="" && typeof sessionInfo!='undefined'
        && sessionInfo!= undefined && sessionInfo!= "undefined" ){
      // hit logout api and remove token
      let logoutInfo = {
        device_token: "fgfd"
      };
      this.http.post(
          `${environment.API_ENDPOINT}${AppSettings.LOGOUT}`, logoutInfo
        )
        .subscribe(
          (data:any) => {
            // console.log("removed");
            this.removeToken();
          },
          err => {
            // console.log("removed errr");
            console.log(err);
            this.removeToken();
          }
      );
    }else{
      // directly remove token
      this.removeToken();
    }

  }

  removeToken(){
    this.cookieService.delete('adminSession', '/');
    this.cookieService.delete('admin',  '/');
    this.loggedIn.next(false);
    this.router.navigate(['/admin/signin']);
  }

  unauthorized(){
    console.log('dffd');
    this.alertService.error("Please login again");
    // this.loggedIn.next(false);
    setTimeout(function(){
        // this.router.navigate(['/login']);
        this.logout();
    }.bind(this),300);
  }

  loginData(){
    let adminInfo = this.cookieService.get('admin');
    if(adminInfo){
        try{
          let adminDetails = JSON.parse(adminInfo);
            if(adminDetails && adminDetails['_id']){
              return adminDetails;
            }else{
              this.logout();
            }
          }catch(ex){
            console.log("json parsing error");
            this.logout();
          }
    }else{
      console.log("Again login to change password");
      this.logout();
    }
  }

}
