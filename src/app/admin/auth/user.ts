export interface User {
  identity: string;
  key: string;
  device_token: string;
}
