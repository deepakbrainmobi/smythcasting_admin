import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-layout',
  templateUrl: './login-layout.component.html',
  styleUrls: ['./login-layout.component.css']
})


export class LoginLayoutComponent implements OnInit{
  public applyColor = true;

  constructor(
    private router: Router
  ){ }

  ngOnInit(){

  }

  applyBgClass(): boolean {
    if (this.router.url === '/terms-and-conditions' || this.router.url === '/privacy-policy' || this.router.url === '/hairdressers-terms-and-conditions') {
        return false;
    }
    return true;
  }

}
