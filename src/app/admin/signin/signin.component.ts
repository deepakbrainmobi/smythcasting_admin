import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LocalAuthService } from '../auth/local-auth.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import {TranslateService} from '@ngx-translate/core'; // --> need to get in ts file

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  form: FormGroup;
  public formSubmitAttempt: boolean = false;
  public showSpinner: boolean = false;
  public errorMessage: any;

  constructor(
    private fb: FormBuilder,
    private localAuthService: LocalAuthService,
    private router: Router,
    private cookieService: CookieService,
    public translate: TranslateService,
    private toastr: ToastrService
    // private socialAuthService: AuthService
  ) {

   }

  ngOnInit() {
    this.form = this.fb.group({
      email:  ['', Validators.compose([Validators.required,
                    Validators.pattern(/(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,9}))+$/)])
              ],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15)])],
    });
  }


  onSubmit() {

    this.formSubmitAttempt = true;
    // this.errorMessage = undefined;
    if (this.form.valid) {
      this.showSpinner = true;
      this.localAuthService.login(this.form.value)
      .subscribe(
        (data:any) => {
          // this.showSpinner = false;
          if (data["statusCode"] ==0 ) {
              this.showSpinner = false;
              if(data["error"]["errorCode"]==5) {
                this.toastr.error(data["error"]["errors"]["message"]);
              } else {
                this.toastr.error(data["error"]["responseMessage"]);
              }
           }
           else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["adminProfile"] && data["responseData"]["accessToken"]){
                   localStorage.setItem( 'adminSession', (data["responseData"]["accessToken"]));
                   localStorage.setItem( 'admin', JSON.stringify(data["responseData"]["adminProfile"]));
                   this.showSpinner = false;
                    this.toastr.success(this.translate.instant('loginSuccess'));
                   this.router.navigate(['/admin/dashboard']);
                   console.log('status 1');
           }
           else{

            console.log('status not 1 nor 0');
            this.showSpinner = false;
            this.toastr.error(this.translate.instant('plsTry'));
           }
          console.log(data);
        },
        err => {
            console.log(err);
            this.showSpinner = false;
            this.toastr.error(this.translate.instant('plsTry'));
        }
      );
    }
  }

}
