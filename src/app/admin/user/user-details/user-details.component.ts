import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { PagerService } from '../../../services/pager.service';
import {Constants} from '../../constants/constants';
import {TranslateService} from '@ngx-translate/core';
import { LocalAuthService } from '../../auth/local-auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit { 

  public search: string; status: number;
  public sortData: any = {};
  public limitPage:Array<number> = Constants.limitPage;
  public showSpinner: boolean = false;
  public pageNo: number;
  public totalCount: any;
  public id: any;
  public image: any;
  public loading = false;

  constructor(
    private pagerService: PagerService,
    private userService: UserService,

    private translate: TranslateService,
    private toastr: ToastrService,
    private localAuthService: LocalAuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  userDetails: any[];

  ngOnInit() {
    this.pageNo=1;
    console.log(this.pageNo, 'pageNo-1');
    this.id = this.route.snapshot.paramMap.get('id')
    this.userDetail();
    // this.workshopDetails(workshops);
  }

  userDetail() {
    this.loading = true;
    this.userService.userDetails(this.id)
        .subscribe( (data:any) => {
          console.log(data);
          if (data["statusCode"] ==0 ) {
              this.loading = false;
              if(data["error"] && data["error"]["errorCode"] == 6){
                this.toastr.error(data["error"]["errors"]["message"]);
                this.router.navigate(['/admin/users'])
              }else if(data["error"] && data["error"]["errorCode"] == 2){
                this.toastr.error(this.translate.instant('unauthorized'));
                return this.localAuthService.unauthorized();
              } else {
               //redirect to back
               this.toastr.error('Unable to fetch user details');
               this.router.navigate(['/admin/users'])
              }
          }
          else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["user"]){
                this.loading = false;
                this.userDetails = data["responseData"]["user"];
          }else{
            console.log('status not 1 nor 0');
            this.loading = false;
            this.router.navigate(['/admin/users'])
          }
        },
        err => {
              this.loading = false;
              this.toastr.error('Unable to fetch workshop details');
              this.router.navigate(['/admin/users']);
          }
    );
  }
}
