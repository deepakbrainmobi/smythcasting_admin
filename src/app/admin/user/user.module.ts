import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoadingModule } from 'ngx-loading';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserListingComponent } from './user-listing/user-listing.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { FilterNamesPipe } from '../../directives/filter-names.pipe';
import { OrderByPipe } from '../../directives/orderby.pipe';
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    LoadingModule,
    FormsModule,
    NgbModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule
  ],
  declarations: [UserComponent, UserListingComponent, UserDetailsComponent,
  					OrderByPipe
  				]
})
export class UserModule { }
