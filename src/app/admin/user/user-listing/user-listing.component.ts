import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/user.service';
import { PagerService } from '../../../services/pager.service';
import {Constants} from '../../constants/constants';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';
import { Pipe, PipeTransform } from '@angular/core';
import { LocalAuthService } from '../../auth/local-auth.service';
import { ToastrService } from 'ngx-toastr';
import {NgForm} from '@angular/forms';
import {FormGroup,FormControl,Validators,FormArray} from '@angular/forms';
import { Router } from '@angular/router';
import { FilterNamesPipe } from '../../../directives/filter-names.pipe';
//import { ModalDirective} from 'ngx-bootstrap';

@Component({
  selector: 'app-user-listing',
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.css'],
})
export class UserListingComponent implements OnInit { 

  // @ViewChild('myModal1') myModal1: ModalDirective;

  public search: string; status: number;
  public sortData: any = {};
  public limitPage:Array<number> = Constants.limitPage;
  public showSpinner: boolean = false;
  public pageNo: number;
  public totalCount: any;
  public loading = false;
  public isAllChecked = false;
  public modalSpinner: boolean = false;
  public displayedColumns = ['S.No', 'Image', 'Name', 'Email', 'Phone', 'Action'];

  records: Array<any>;
  isDesc: boolean = false;
  column: string = 'firstName';
  direction: number;

  constructor(
    private pagerService: PagerService,
    private userService: UserService,
    private translate: TranslateService,
    private toastr: ToastrService,
    private localAuthService: LocalAuthService,
    private router: Router,
    private modalService: NgbModal,
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  users: any[];

  ngOnInit() {
    this.pageNo=1;
    console.log(this.pageNo, 'pageNo-1');
    this.userListing(this.search, this.pageNo , this.limitPage[0], {});
  }


  // modal1() {
  //   this.myModal1.show();
  // }

  userListing(search, pageNo, limitPage, sortInfo){
    this.loading = true;
    this.userService.userListing(search, pageNo, limitPage, sortInfo)
          .subscribe( (data: any) => {
            if (data["status"] ==0 ) {
               if(data["error"] && data["error"]["errorCode"] == 2){
                    this.toastr.error(this.translate.instant('unauthorized'));
                    return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3 ||  data["error"]["errorCode"] == 5)){
                    this.toastr.error(this.translate.instant('plsTry'));
                }else{
                 this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["userListing"]){
                 
                  this.users = this.records = data["responseData"]["userListing"];
             }
             else{
              console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             console.log(data);
             this.loading = false;
          },
          err => {
              this.loading = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
  }

  userDelete(user: any){
    if(confirm("Are you sure, you want to delete "+user.firstName+"?")) {
      this.userService.userDelete(user._id)
          .subscribe( (data: any) => {
            if (data["status"] ==0 ) {
               if(data["error"] && data["error"]["errorCode"] == 2){
                    this.toastr.error(this.translate.instant('unauthorized'));
                    return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3 ||  data["error"]["errorCode"] == 5)){
                    this.toastr.error(this.translate.instant('plsTry'));
                }else{
                 this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]){
                  this.toastr.success(data["responseData"]["message"]);
                  this.userListing(this.search, 1, this.limitPage[0], {});
             }
             else{
              console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             console.log(data);

          },
          err => {
              this.showSpinner = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
      );
    } 
  }


  sendEmailToUsers() {
    this.toastr.success("Mail sent successfully.");
    this.router.navigate(['/admin/users']);
  }

  /*
  * Sort User Data
  *
  */
  sort(property){
    this.isDesc = !this.isDesc; //change the direction    
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  /*
  * Multiselect User
  *
  */
  CheckUncheckHeader($event) {
    // console.log(this.users.length, "========", $event);
      this.isAllChecked = true;
      for (var i = 0; i < this.users.length; i++) {
        console.log(this.users[i], "========");
          if (!this.users[i].Selected) {
              this.isAllChecked = false;
              break;
          }
      };
  };

  //CheckUncheckHeader();
  CheckUncheckAll($event) {
      for (var i = 0; i < this.users.length; i++) {
        console.log(this.users[0], "++++++++++++++", $event);
        this.users[i].Selected = this.isAllChecked;
      }
  };

  /**
   * Modal Service
   *
   */
  chkClick(modalContent, type: string) {
    //if(type === 'country'){
      //this.countryListing();
      console.log(modalContent, "dfdfdfdfds");
      this.openModal(modalContent);
    //}
    // else if(type === 'city'){
    //   if(this.workCountry){
    //     this.cityListing();
    //     this.openModal(modalContent);
    //   }else{
    //     this.toastr.error("Please select country first then choose city");
    //   }
    // }else if(type === 'province'){
    //   if(this.taxCountry){
    //     this.provinceListing();
    //     this.openModal(modalContent);
    //   }else{
    //   this.toastr.error("Please select country first then choose province");
    //   }
    // }
  }

  openModal(modalContent){
    console.log(modalContent, "=====================");
     this.modalService.open(modalContent, { centered: true});
   }

}