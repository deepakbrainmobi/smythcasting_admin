import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserListingComponent } from './user-listing/user-listing.component';
import { UserDetailsComponent } from './user-details/user-details.component';

const routes: Routes = [
  {
   path: '',
     children: [
       { path: '', component: UserListingComponent },
       { path: 'view/:id', component: UserDetailsComponent },
     ]
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
