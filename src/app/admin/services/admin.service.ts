import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {map, catchError} from "rxjs/operators";
import {AppSettings} from './app-settings';
import { LocalAuthService } from '../auth/local-auth.service';

import {environment} from '../../../environments/environment';


@Injectable()
export class AdminService {



    constructor(private http:HttpClient, private authService: LocalAuthService) {

    }




    forgotPassword(adminData: any){
      console.log(`${environment.API_ENDPOINT}${AppSettings.FORGOT_PASS}`);
      return this.http.post(
          `${environment.API_ENDPOINT}${AppSettings.FORGOT_PASS}`, adminData
        );
    }



    matchTokenSavePass(matchTknPasInfo: any){
      console.log(`${environment.API_ENDPOINT}${AppSettings.RESET_PASS}`);
      return this.http.post(
          `${environment.API_ENDPOINT}${AppSettings.RESET_PASS}`, matchTknPasInfo
        );
    }
    dashboardCount(){
      console.log(`${environment.API_ENDPOINT}${AppSettings.DASHBOARD_COUNT}`);
      return this.http.get(
          `${environment.API_ENDPOINT}${AppSettings.DASHBOARD_COUNT}`
        );
    }


}
