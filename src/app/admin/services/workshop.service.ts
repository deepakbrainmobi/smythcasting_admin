import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {map, catchError} from "rxjs/operators";
import {AppSettings} from './app-settings';
import { LocalAuthService } from '../auth/local-auth.service';

import {environment} from '../../../environments/environment';


@Injectable()
export class WorkshopService {

    constructor(private http:HttpClient, private authService: LocalAuthService) {

    }

    //All Workshop
    workshopListing(filter: string = '',
        pageNumber: number=1 , pageSize: Number = 50, sortData: any) {
        return this.http.get(`${environment.API_ENDPOINT}${AppSettings.WORKSHOP_LISTING}`,{
              params: new HttpParams()
                  .set('page_no', pageNumber.toString())
                  .set('limit_page', pageSize.toString())
          }
        )
    }

    //Workshop By Id
    workshopDetails(id:string) {
      return this.http.get(`${environment.API_ENDPOINT}${AppSettings.WORKSHOP_DETAILS}${id}`)
    }

    // salonAdd(addressData: any, latLng: any, salonInfo: any, selectedOwner: any, imgInfo: any){
    //   return this.http.post(`${environment.API_ENDPOINT}${AppSettings.SALON_ADD}`,
    //     this.freezeSalonObject(addressData, latLng, salonInfo, selectedOwner, 0, imgInfo)
    //   );
    // }


    // salonEdit(addressData: any, latLng: any, salonInfo: any, id: string,  selectedOwner: any, imgInfo: any){
    //   return this.http.put(`${environment.API_ENDPOINT}${AppSettings.SALON_UPDATE}${id}`,
    //     this.freezeSalonObject(addressData, latLng, salonInfo, selectedOwner, 1, imgInfo)
    //   );
    // }

    //Create Workshop 
    addWorkshop(addressData: any, latLng: any, formValue: any, image: any){
      console.log(formValue, "formValue");
      console.log(latLng, "LATLONG SET");
      const formData: FormData = new FormData();
      formData.append('title', formValue.title);
      formData.append('description', formValue.description);
      formData.append('startDate', ((formValue.startDate) ? formValue.startDate.year+'-'+(("0" + formValue.startDate.month).slice(-2))+'-'+(("0" + formValue.startDate.day).slice(-2)) : ''));
      formData.append('endDate', ((formValue.endDate) ? formValue.endDate.year+'-'+(("0" + formValue.endDate.month).slice(-2))+'-'+(("0" + formValue.endDate.day).slice(-2)):''));
      formData.append('seats', formValue.seats);
      formData.append('cost', formValue.cost);
      formData.append('category', formValue.category);
      formData.append('venue', formValue.venue);
      formData.append('address', addressData.formattedAddress);
      formData.append('venuePhone', formValue.venuePhone);
      formData.append('organizer', formValue.organizer);
      formData.append('phone', formValue.phone);
      formData.append('website', formValue.website);
      formData.append('latitude', latLng.lat);
      formData.append('longitude', latLng.lng);
      // formData.append('latitude', '43.6532');
      // formData.append('longitude', '79.3832');
      formData.append('email', formValue.email);

      if(image)
        formData.append('image', image, image.name);

      console.log(formData, "Workshop Form Data");
      
      return this.http.post(`${environment.API_ENDPOINT}${AppSettings.WORKSHOP_CREATE}`,formData);
    }

    updateWorkshop(addressData: any, latLng: any, id:string, formValue: any, image: any){
      console.log(addressData, "ADRESS DATA EDIT");
      console.log(latLng, "LATLONG SET EDIT");
      const formData: FormData = new FormData();
      formData.append('title', formValue.title);
      formData.append('description', formValue.description);
      formData.append('startDate', ((typeof formValue.startDate !='undefined') ? formValue.startDate.year+'-'+(("0" + formValue.startDate.month).slice(-2))+'-'+(("0" + formValue.startDate.day).slice(-2)) : ''));
      formData.append('endDate', ((typeof formValue.endDate !='undefined') ? formValue.endDate.year+'-'+(("0" + formValue.endDate.month).slice(-2))+'-'+(("0" + formValue.endDate.day).slice(-2)):''));
      formData.append('seats', formValue.seats);
      formData.append('cost', formValue.cost);
      formData.append('category', formValue.category);
      formData.append('venue', formValue.venue);
      formData.append('address', addressData.formattedAddress);
      formData.append('venuePhone', formValue.venuePhone);
      formData.append('organizer', formValue.organizer);
      formData.append('phone', formValue.phone);
      formData.append('website', formValue.website);
      formData.append('latitude', latLng.lat);
      formData.append('longitude', latLng.lng);
      formData.append('email', formValue.email);
      if(image)
        formData.append('image', image, image.name);

      return this.http.put(`${environment.API_ENDPOINT}${AppSettings.WORKSHOP_UPDATE}${id}`,formData);
    }

    //Delate Workshop
    deleteWorkshop(id:string){
      return this.http.delete(`${environment.API_ENDPOINT}${AppSettings.WORKSHOP_DELETE}${id}`);
    }


}
