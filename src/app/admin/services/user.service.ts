import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {map, catchError} from "rxjs/operators";
import {AppSettings} from './app-settings';
import { LocalAuthService } from '../auth/local-auth.service';

import {environment} from '../../../environments/environment';


@Injectable()
export class UserService {

    constructor(private http:HttpClient, private authService: LocalAuthService) {

    }

    userListing(filter: string = '',
        pageNumber: number=1 , pageSize: Number = 50, sortData: any) {
        return this.http.get(`${environment.API_ENDPOINT}${AppSettings.USER_LISTING}`,{
              params: new HttpParams()
                  .set('page_no', pageNumber.toString())
                  .set('limit_page', pageSize.toString())
          }
        )
    }

    userDetails(id:string){
      return this.http.get(`${environment.API_ENDPOINT}${AppSettings.USER_DETAILS}${id}`);
    }

    userDelete(id:string){
      return this.http.delete(`${environment.API_ENDPOINT}${AppSettings.USER_DELETE}${id}`);
    }


}
