export class AppSettings {

   public static LOGIN: string = 'smyth/api/v2/admin/login';
   public static FORGOT_PASS: string = 'smyth/api/v2/admin/forgotPassword';
   public static RESET_PASS: string = 'smyth/api/v2/admin/resetPassword';
   public static LOGOUT: string = 'smyth/api/v2/admin/logout';
   public static DASHBOARD_COUNT: string = 'smyth/api/v2/admin/dashboardCount';

   public static USER_LISTING: string = 'smyth/api/v2/user/listing';
   public static USER_DETAILS: string = 'smyth/api/v2/user/';
   public static USER_DELETE: string = 'smyth/api/v2/user/';

   public static CAT_CREATE: string = 'smyth/api/v2/category/create';
   public static CAT_LISTING: string = 'smyth/api/v2/category/listing';
   public static CAT_DELETE: string = 'smyth/api/v2/category';

   public static WORKSHOP_CREATE: string = 'smyth/api/v2/workshop/create';
   public static WORKSHOP_UPDATE: string = 'smyth/api/v2/workshop/';
   public static WORKSHOP_LISTING: string = 'smyth/api/v2/workshop/listing';
   public static WORKSHOP_DETAILS: string = 'smyth/api/v2/workshop/';
   public static WORKSHOP_DELETE: string = 'smyth/api/v2/workshop/';

   public static WEBPAGE_CREATE: string = 'smyth/api/v2/page/create';
   public static WEBPAGE_UPDATE: string = 'smyth/api/v2/page/';
   public static WEBPAGE_LISTING: string = 'smyth/api/v2/page/listing';
   public static WEBPAGE_DETAILS: string = 'smyth/api/v2/page/';
   public static WEBPAGE_DELETE: string = 'smyth/api/v2/page/';

}
