import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {map, catchError} from "rxjs/operators";
import {AppSettings} from './app-settings';
import { LocalAuthService } from '../auth/local-auth.service';

import {environment} from '../../../environments/environment';


@Injectable()
export class WebpageService {

    constructor(private http:HttpClient, private authService: LocalAuthService) {

    }

    //All Webpage
    webpageListing(filter: string = '',
        pageNumber: number=1 , pageSize: Number = 50, sortData: any) {
        return this.http.get(`${environment.API_ENDPOINT}${AppSettings.WEBPAGE_LISTING}`,{
              params: new HttpParams()
                  .set('page_no', pageNumber.toString())
                  .set('limit_page', pageSize.toString())
          }
        )
    }

    //Webpage By Id
    webpageDetails(id:string) {
      return this.http.get(`${environment.API_ENDPOINT}${AppSettings.WEBPAGE_DETAILS}${id}`)
    }

    //Create Webpage 
    addWebpage(formValue: any, image: any){
      const formData: FormData = new FormData();
      formData.append('title', formValue.title);
      formData.append('description', formValue.description);
      formData.append('short_desc', formValue.short_desc);

      if(image)
        formData.append('image', image, image.name);

      return this.http.post(`${environment.API_ENDPOINT}${AppSettings.WEBPAGE_CREATE}`,formData);
    }

    updateWebpage(id:string, formValue: any, image: any){
      const formData: FormData = new FormData();
      formData.append('title', formValue.title);
      formData.append('description', formValue.description);
      formData.append('short_desc', formValue.short_desc);

      if(image)
        formData.append('image', image, image.name);

      return this.http.put(`${environment.API_ENDPOINT}${AppSettings.WEBPAGE_UPDATE}${id}`,formData);
    }

    //Delate Webpage
    deleteWebpage(id:string){
      return this.http.delete(`${environment.API_ENDPOINT}${AppSettings.WEBPAGE_DELETE}${id}`);
    }
}
