import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminRoutingModule } from './/admin-routing.module';
import { AdminComponent } from './admin.component';
import { SigninComponent } from './signin/signin.component';

import { NavbarModule } from './navbar/navbar.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { LoginLayoutComponent } from './layouts/login-layout.component';
import { HomeLayoutComponent } from './layouts/home-layout.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {TranslateModule} from '@ngx-translate/core';

import { AuthGuard } from './auth/auth.guard';
import { LocalAuthService } from './auth/local-auth.service';
import { AlertService } from './services/alert.service';
import { AlertComponent } from './alerts/alert.component';

import { CookieService } from 'ngx-cookie-service';
import { AdminService } from './services/admin.service';
import { UserService } from './services/user.service';
import { WorkshopService } from './services/workshop.service';
import { WebpageService } from './services/webpage.service';

import { UserModule } from './user/user.module';
import { WorkshopModule } from './workshop/workshop.module';
import { WebpageModule } from './webpage/webpage.module';
import { LoadingModule } from 'ngx-loading';

// import {OrderByPipe} from '../directives/orderby.pipe';

@NgModule({
  imports: [
    NgbModule.forRoot(),
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    NavbarModule,
    SidebarModule,
    WorkshopModule,
    WebpageModule,
    LoadingModule,   
   ],
  declarations: [LoginLayoutComponent, HomeLayoutComponent, AdminComponent, SigninComponent, FooterComponent, DashboardComponent,
                  ForgotPasswordComponent, ResetPasswordComponent, AlertComponent, 
                  // OrderByPipe
                  ],
  providers:[
    LocalAuthService, AuthGuard, CookieService, 
    AlertService, AdminService, WorkshopService, UserService,
    WebpageService

  ]
})
export class AdminModule { }
