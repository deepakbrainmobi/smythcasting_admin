import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';// --> of admin
import { AdminComponent } from './admin.component';
import { SigninComponent } from './signin/signin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginLayoutComponent } from './layouts/login-layout.component';
import { HomeLayoutComponent } from './layouts/home-layout.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [

  {
    path: '',component: AdminComponent,
    children: [
      {
        path: '',
        component: HomeLayoutComponent,
        canActivate: [AuthGuard],
        children: [
          { path: '', component: DashboardComponent },
          { path: 'dashboard', component: DashboardComponent },
          { path: 'users', loadChildren: "./user/user.module#UserModule"},
          { path: 'workshops', loadChildren: "./workshop/workshop.module#WorkshopModule"},
          { path: 'webpages', loadChildren: "./webpage/webpage.module#WebpageModule"},
         ]
      },

      {
        path: '',
        component: LoginLayoutComponent,
        children: [

            { path: '', component: SigninComponent },
            { path: 'signin', component: SigninComponent },
            { path: 'forgot-password', component: ForgotPasswordComponent },
            { path: 'reset/password/:token', component: ResetPasswordComponent },

        ]
      }




    ]
  }
];


@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]

})
export class AdminRoutingModule { }
