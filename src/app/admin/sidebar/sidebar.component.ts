import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [

    { path: '/admin/dashboard', title: 'Dashboard',  icon:'pe-7s-home', class: ''  },
    { path: '/admin/users', title: 'Users',  icon:'pe-7s-user', class: ''  },
    { path: '/admin/workshops', title: 'Workshops',  icon:'pe-7s-news-paper', class: ''  },
    // { path: '/admin/webpages', title: 'Webpages',  icon:'pe-7s-map-2', class: ''  },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      return true;
  };
}
