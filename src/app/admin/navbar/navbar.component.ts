import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../sidebar/sidebar.component';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

import { LocalAuthService } from '../auth/local-auth.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
    // moduleId: module.id,
    selector: 'navbar-cmp',
    templateUrl: 'navbar.component.html'
})

export class NavbarComponent implements OnInit{
    private listTitles: any[];
    location: Location;
    private toggleButton: any;
    private sidebarVisible: boolean;
    public admin: any;

    constructor(location: Location,
      private element: ElementRef,
      private localAuthService: LocalAuthService,
      private cookieService: CookieService) {
      this.location = location;
          this.sidebarVisible = false;
    }

    ngOnInit(){
      this.listTitles = ROUTES.filter(listTitle => listTitle);
      // console.log(this.listTitles);
      const navbar: HTMLElement = this.element.nativeElement;
      this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        try {
          this.admin = JSON.parse(localStorage.getItem("admin"));
        } catch(e) {
            // alert(e); // error in the above string (in this case, yes)!
          this.admin = {}
        }
      // console.log(this.admin);
      // console.log('this.admin');
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function(){
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    sidebarToggle() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };

    getTitle(): string{
      var titlee = this.location.prepareExternalUrl(this.location.path());
      // console.log(this.location.prepareExternalUrl(this.location.path()));
      titlee = titlee.split('/').pop();
      for(var item = 0; item < this.listTitles.length; item++){
          if(this.listTitles[item].path === ("/"+titlee)){
              return this.listTitles[item].title;
          }
      }
      // Dashboard  is Categories
      return 'Categories';
    }

    onLogout(){
        if(confirm("Are you sure, you want to logout?")) {
          localStorage.clear();
          this.localAuthService.logout();
        }
    }

}
