import { Component, OnInit, ViewChild } from '@angular/core';
import { PasswordValidation } from '../../validators/password-validation';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { AdminService } from '../services/admin.service';
import {TranslateService} from '@ngx-translate/core'; // --> need to get in ts file

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  form: FormGroup;
  public formSubmitAttempt: boolean = false;
  public showSpinner: boolean = false;
  // public email: any;
  public token: any;
  public errorMessage: any;
  @ViewChild('f') myForm;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private adminService: AdminService,
    private route: ActivatedRoute,

    public translate: TranslateService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getAdminDetails();
    this.form = this.fb.group({
        new_password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15)])],
        cnf_password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15)])]
      },
      {
         validator: [PasswordValidation.MatchPassword] // your validation method
      }
    );
  }


  getAdminDetails(){
     this.token = this.route.snapshot.paramMap.get('token');
     console.log(this.token);
  }

  onSubmit() {
    this.formSubmitAttempt = true;
    // this.errorMessage = undefined;
    if (this.form.valid) {
      this.showSpinner = true;
      let matchTknPasInfo = {
        // email: this.email,
        reset_token: this.token,
        password: this.form.value.new_password,
      };
      this.adminService.matchTokenSavePass(matchTknPasInfo)
      .subscribe(
        (data:any) => {
          this.showSpinner = false;
          if (data["statusCode"] ==0 ) {
             if(data["error"] && (data["error"]["errorCode"] == 25)){
               this.toastr.error(data["error"]["responseMessage"]);
             }else if(data["error"] && (data["error"]["errorCode"] == 6)){//4 for email and 6 for phonenumber
                   this.toastr.error(this.translate.instant('plsTry'));
             }else{
               this.toastr.error(this.translate.instant('plsTry'));
             }
           }
           else if (data["statusCode"] ==1){
                  console.log('status 1');
                  this.myForm.resetForm();

                  this.toastr.success(data["responseData"]["message"]);
                  this.router.navigate(['/admin/signin']);
           }
           else{
            console.log('status not 1 nor 0');
            this.toastr.error(this.translate.instant('plsTry'));
           }
          console.log(data);
        },
        err => {
            this.showSpinner = false;
            console.log(err);
            this.toastr.error(this.translate.instant('plsTry'));
        }
      );
    }
  }



}
