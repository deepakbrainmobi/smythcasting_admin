import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(
    private cookieService: CookieService,
    public translate: TranslateService
  ) {
    translate.addLangs(['en', 'fr', 'es']);
    translate.setDefaultLang('en');

    // const browserLang = translate.getBrowserLang();
    const browserLang = 'en';
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
  }

  ngOnInit() {
  }

}
