import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	public usersCount: any = 0;
	public workshopCount: any = 0;
  public loading = false;

	constructor(
		private adminService: AdminService
	) { 
		
	}

  	ngOnInit() {
  		this.dashboardCount();
  	}

  	dashboardCount(){
    this.loading = true;
    this.adminService.dashboardCount()
          .subscribe( (data: any) => {
          	//console.log(data);
            if (data["statusCode"] ==0 ) {
               
             }
             else if (data["statusCode"] ==1 && data["responseData"]){
                 
                  this.usersCount = data["responseData"]["users"];
                  this.workshopCount = data["responseData"]["workshops"];
             }
             else{
              console.log('status not 1 nor 0');
             }
             console.log(data);
            this.loading = false;
          },
          err => {
              // this.showSpinner = false;
              // this.toastr.error(this.translate.instant('plsTry'));
          }
        );
  }

}
