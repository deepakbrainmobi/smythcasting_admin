import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoadingModule } from 'ngx-loading';
import { AgmCoreModule } from '@agm/core';

import { WorkshopRoutingModule } from './workshop-routing.module';
import { WorkshopComponent } from './workshop.component';
import { WorkshopListingComponent } from './workshop-listing/workshop-listing.component';
import { WorkshopAddComponent } from './workshop-add/workshop-add.component';
import { WorkshopDetailsComponent } from './workshop-details/workshop-details.component';

@NgModule({
  imports: [
  	NgbModule,
    CommonModule,
    WorkshopRoutingModule,
    ReactiveFormsModule,
    LoadingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBy4tBxJdnd9t7hsNf-mLbuhNdVOuCy_P4',
      libraries : ['places']
    }),
  ],
  declarations: [WorkshopComponent, WorkshopListingComponent, WorkshopAddComponent,
  				 WorkshopDetailsComponent]
})
export class WorkshopModule { }
