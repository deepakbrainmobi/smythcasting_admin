import { Component, OnInit } from '@angular/core';
import { WorkshopService } from '../../services/workshop.service';
import { PagerService } from '../../../services/pager.service';
import {Constants} from '../../constants/constants';

import {TranslateService} from '@ngx-translate/core';

import { LocalAuthService } from '../../auth/local-auth.service';
import { ToastrService } from 'ngx-toastr';

import { Router } from '@angular/router';

@Component({
  selector: 'app-workshop-listing',
  templateUrl: './workshop-listing.component.html',
  styleUrls: ['./workshop-listing.component.css']
})
export class WorkshopListingComponent implements OnInit { 

  public search: string; status: number;
  public sortData: any = {};
  public limitPage:Array<number> = Constants.limitPage;
  public showSpinner: boolean = false;
  public pageNo: number;
  public totalCount: any;
  public loading = false;
  public displayedColumns = ['S.No', 'Title', 'Date', 'Venue', 'Organizer', 'Phone', 'Venue Phone', 'Action'];

  constructor(
    private pagerService: PagerService,
    private WorkshopService: WorkshopService,

    private translate: TranslateService,
    private toastr: ToastrService,
    private localAuthService: LocalAuthService,
    private router: Router
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  workshops: any[];

  ngOnInit() {
    this.pageNo=1;
    console.log(this.pageNo, 'pageNo-1');
    this.workshopListing(this.search, this.pageNo , this.limitPage[0], {});
  }

  workshopListing(search, pageNo, limitPage, sortInfo){
    this.loading = true;
    this.WorkshopService.workshopListing(search, pageNo, limitPage, sortInfo)
          .subscribe( (data: any) => {
            if (data["status"] ==0 ) {
              this.loading = false;
               if(data["error"] && data["error"]["errorCode"] == 2){
                    this.toastr.error(this.translate.instant('unauthorized'));
                    return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3 ||  data["error"]["errorCode"] == 5)){
                    this.toastr.error(this.translate.instant('plsTry'));
                }else{
                 this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["workshopListing"]){
                this.loading = false;   
                this.workshops = data["responseData"]["workshopListing"];
             }
             else{
              //console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             //console.log(data);
            this.loading = false;
          },
          err => {
              this.loading = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
  }

  deleteWorkshop(workshop: any){
    if(confirm("Are you sure, you want to delete "+workshop.title+"?")) {
      this.WorkshopService.deleteWorkshop(workshop._id)
          .subscribe( (data: any) => {
            if (data["status"] ==0 ) {
               if(data["error"] && data["error"]["errorCode"] == 2){
                    this.toastr.error(this.translate.instant('unauthorized'));
                    return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3 ||  data["error"]["errorCode"] == 5)){
                    this.toastr.error(this.translate.instant('plsTry'));
                }else{
                 this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]){
                  this.toastr.success(data["responseData"]["message"]);
                  this.workshopListing(this.search, 1 , this.limitPage[0], {});
             }
             else{
              //console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             //console.log(data);

          },
          err => {
              this.showSpinner = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
    }
  }

}
