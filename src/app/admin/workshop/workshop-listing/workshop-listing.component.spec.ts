import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopListingComponent } from './workshop-listing.component';

describe('WorkshopListingComponent', () => {
  let component: WorkshopListingComponent;
  let fixture: ComponentFixture<WorkshopListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
