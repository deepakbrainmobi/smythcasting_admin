import { Component, OnInit } from '@angular/core';
import { WorkshopService } from '../../services/workshop.service';
import { PagerService } from '../../../services/pager.service';
import {Constants} from '../../constants/constants';

import {TranslateService} from '@ngx-translate/core';

import { LocalAuthService } from '../../auth/local-auth.service';
import { ToastrService } from 'ngx-toastr';

import { Router } from '@angular/router';

@Component({
  selector: 'app-workshop-details',
  templateUrl: './workshop-details.component.html',
  styleUrls: ['./workshop-details.component.css']
})
export class WorkshopDetailsComponent implements OnInit { 

  public search: string; status: number;
  public sortData: any = {};
  public limitPage:Array<number> = Constants.limitPage;
  public showSpinner: boolean = false;
  public pageNo: number;
  public totalCount: any;
  public displayedColumns = ['S.No', 'Title', 'Start Date', 'Venue', 'Organizer', 'Phone', 'Venue Phone', 'Created', 'Action'];

  constructor(
    private pagerService: PagerService,
    private WorkshopService: WorkshopService,

    private translate: TranslateService,
    private toastr: ToastrService,
    private localAuthService: LocalAuthService,
    private router: Router
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  workshops: any[];

  ngOnInit() {
    this.pageNo=1;
    console.log(this.pageNo, 'pageNo-1');
    // this.workshopDetails(workshops);
  }

  // workshopDetails(workshops: any){
  //   this.WorkshopService.workshopDetails(workshops,_id)
  //         .subscribe( (data: any) => {
  //           if (data["status"] ==0 ) {
  //              if(data["error"] && data["error"]["errorCode"] == 2){
  //                   this.toastr.error(this.translate.instant('unauthorized'));
  //                   return this.localAuthService.unauthorized();
  //              }else if(data["error"] && (data["error"]["errorCode"] == 3 ||  data["error"]["errorCode"] == 5)){
  //                   this.toastr.error(this.translate.instant('plsTry'));
  //               }else{
  //                this.toastr.error(this.translate.instant('plsTry'));
  //              }
  //            }
  //            else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["workshopDetails"]){
                 
  //                 this.workshops = data["responseData"]["workshopDetails"];
  //            }
  //            else{
  //             //console.log('status not 1 nor 0');
  //             this.toastr.error(this.translate.instant('plsTry'));
  //            }
  //            //console.log(data);

  //         },
  //         err => {
  //             this.showSpinner = false;
  //             this.toastr.error(this.translate.instant('plsTry'));
  //         }
  //       );
  // }
}
