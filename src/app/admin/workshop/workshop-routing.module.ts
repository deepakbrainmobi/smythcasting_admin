import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkshopComponent } from './workshop.component';

import { WorkshopListingComponent } from './workshop-listing/workshop-listing.component';
import { WorkshopAddComponent } from './workshop-add/workshop-add.component';
import { WorkshopDetailsComponent } from './workshop-details/workshop-details.component';
const routes: Routes = [
  {
   path: '', component: WorkshopComponent ,
     children: [
       { path: '', component: WorkshopListingComponent },
       { path: 'add', component: WorkshopAddComponent },
       { path: 'edit/:id', component: WorkshopAddComponent },
       { path: ':id', component: WorkshopDetailsComponent },
     ]
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkshopRoutingModule { }
