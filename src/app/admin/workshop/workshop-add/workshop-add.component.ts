import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { WorkshopService } from '../../services/workshop.service';
import { PagerService } from '../../../services/pager.service';
import {Constants} from '../../constants/constants';
import { Location } from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import { LocalAuthService } from '../../auth/local-auth.service';
import { ToastrService } from 'ngx-toastr';
import {map} from 'rxjs/operators/map';
import { Router, ActivatedRoute } from '@angular/router';
import {NgForm} from '@angular/forms';
import {FormGroup,FormControl,Validators,FormArray} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import { AgmCoreModule, MapsAPILoader  } from '@agm/core';
import { DateFunctions } from '../../../shared/date-functions';

declare var google: any;

@Component({
  selector: 'app-workshop-add',
  templateUrl: './workshop-add.component.html',
  styleUrls: ['./workshop-add.component.css'],
})

export class WorkshopAddComponent implements OnInit { 

  public WorkshopForm:FormGroup;;
  public showSpinner: boolean = false;
  public image: any;
  public id: any;
  map:any;
  public today: number = Date.now();
  public serverImage: any;
  public formSubmitAttempt: boolean = false;
  public loading = false;
  public searchNow: boolean = false;

  myMarker: marker = {
      lat:  43.6532,
      lng:  79.3832,
      label: '',
      draggable: true
  }
  addressFields: any;
  oldData: any;

  @ViewChild("search") public searchElementRef: ElementRef;
  
  constructor(
    private pagerService: PagerService,
    private workshopService: WorkshopService,
    private mapsAPILoader: MapsAPILoader,
    private translate: TranslateService,
    private toastr: ToastrService,
    private localAuthService: LocalAuthService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private ngZone: NgZone,
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};
  chkStartDate: any = {};

  // paged items
  workshops: any[];
  minDate: any;
  //this.maxDate = DateFunctions.subtractNgbYear(DateFunctions.ngbTodayDate(), 5)// difference of 5 years

  //Initialization
  ngOnInit() {

    this.formSubmitAttempt = false;
    this.minDate = DateFunctions.ngbTodayDate();// difference of 5 years
    console.log(this.minDate);
    //console.log(this.router.url, 'this.router.url');
    this.initForm();
    if(this.router.url == '/admin/workshops/add' || this.router.url == '/workshops/add') {
      // this.initForm();
    }else{
      this.id = this.route.snapshot.paramMap.get('id') || '0';
      if(this.id!='0'){
        this.getWorkshop();
      }else{
        this.toastr.error("No Details Found.");
      }
    }
  }

  //Initialize Form
  initForm() {
    this.WorkshopForm = new FormGroup({
      'workshopData': new FormGroup({
          'title':new FormControl(null,[Validators.required,Validators.maxLength(50),Validators.pattern('^[a-zA-Z ]+$')]),
          'description':new FormControl(null,[Validators.required]),
          'startDate':new FormControl(null,[Validators.required]),
          'endDate':new FormControl(null,[Validators.required]),
          'seats':new FormControl(null,[Validators.required,Validators.maxLength(3),Validators.pattern('^[0-9]+$')]),
          'cost':new FormControl(null,[Validators.required,Validators.maxLength(8),Validators.pattern('^[-+]?[0-9]+(\.[0-9]+)?$')]),
          'category':new FormControl(null),
          'venue':new FormControl(null,[Validators.required]),
          'image':new FormControl(null),
          //'address':new FormControl(null,[Validators.required]),
          'venuePhone':new FormControl(null,[Validators.required,Validators.maxLength(10),Validators.pattern('^[0-9]+$')]),
          'organizer':new FormControl(null,[Validators.required]),
          'phone':new FormControl(null,[Validators.required,Validators.maxLength(10),Validators.pattern('^[0-9]+$')]),
          'website':new FormControl(null),
          'searchControl': new FormControl(null, [Validators.required]),
          // 'latitude':new FormControl(null),
          // 'longitude':new FormControl(null),
          'email':new FormControl(null,[Validators.required,Validators.email]),
      })
    });
  }

  //Read Url For Image
  readUrl(event:any) {
    console.log(event.target.files[0].type, "===========");
    if (event.target.files && event.target.files[0]) {
      //check for image format
      if(event.target.files[0].type == "image/jpeg" || event.target.files[0].type == "image/jpg" || event.target.files[0].type == "image/png") {
        var reader = new FileReader();
        reader.onload = (event:any) => {
          this.serverImage = event.target.result;
        }
        // console.log(event.target.files[0]);
        this.image = event.target.files[0];
        reader.readAsDataURL(event.target.files[0]);
      } else {
        this.toastr.error('Wrong image type. Only jpeg|jpg|png are allowed.');
      }
    }
  }

  //Submit Event
  onSubmit() {
    this.formSubmitAttempt = true;
    // console.log(this.minDate.day);
    // console.log(this.WorkshopForm.value.workshopData.startDate.day);
    this.chkStartDate = this.WorkshopForm.value.workshopData.startDate.day;
    if(this.minDate.day > this.chkStartDate) {
       this.toastr.error(this.translate.instant('Please select valid start date.'));
    }

    if (this.WorkshopForm.valid) {
      // if( !this.addressFields || !this.addressFields.administrativeAreaLevel1 || !this.addressFields.administrativeAreaLevel2 ||
      //     !this.addressFields.countryShortName || !this.addressFields.countryLongName || !this.addressFields.formattedAddress ||
      //     !this.addressFields.postalCode || !this.myMarker.lat || !this.myMarker.lng){
      //      return;
      // }
      if(this.router.url == '/admin/workshops/add' || this.router.url == '/workshops/add') {        
        this.createWorkshop()
      }else{
        console.log("Update my form");
        this.updateWorkshop()  
      }
    }
  }

  mapReady(event: any) {
    this.mapLoad(event);
      if(this.id=='0')
        this.reverseGeoCoding(event);
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    // console.log('dragEnd', m, $event);
    this.setLatLng($event);
    this.reverseGeoCoding(event);
  }

  mapClicked($event: any) {
    // console.log($event);
    // console.log($event.coords.lat);
    // console.log($event.coords.lng);
    this.setLatLng($event);
  }

  setLatLng($event){
    this.myMarker= {
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    };
    // console.log("simple event",event);
    this.reverseGeoCoding(event);
  }

  clickedMarker(label: string, index: number) {
    // console.log(`clicked the marker: ${label || index}`)
  }

  //Load google map
  mapLoad(event){
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place = autocomplete.getPlace();
          console.log(place, "=============");
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            console.log(this.WorkshopForm.controls.workshopData['controls'].searchControl, "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
            this.WorkshopForm.controls.workshopData['controls'].searchControl.setValue("");
            return;
          }
          this.setAddressParams(place);
          this.myMarker= {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng(),
            draggable: true
          };
        });
      });
    });
  }

  reverseGeoCoding($event){
    let geocoder = new google.maps.Geocoder();
    let latlng = new google.maps.LatLng(this.myMarker.lat, this.myMarker.lng);
    let request = {
      latLng: latlng
    };
    geocoder.geocode(request, (results, status) => {
      // this.ngZone.run(() => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0] != null) {
            // console.log('geocode');
            // console.log(results[0]);
            this.WorkshopForm.controls.workshopData['controls'].searchControl.setValue(results[0].formatted_address);
           
            this.setAddressParams(results[0]);
          } else {
            this.WorkshopForm.controls.workshopData['controls'].searchControl.setValue("");
            // alert("No address available");
            // this.searchControl= "";
          }
          // console.log(this.searchControl,'this.searchControl');
        }else{
        this.WorkshopForm.controls.workshopData['controls'].searchControl.setValue("");
      }
      // });
    });
  }

  //Set Address Params
  setAddressParams(results){
    this.addressFields = {
      formattedAddress: results.formatted_address,
      administrativeAreaLevel1: "",
      administrativeAreaLevel2: "",
      countryShortName: "",
      countryLongName: "",
      postalCode: ""
    };
    for(let k=0;k<results.address_components.length;k++){
      let typeData = results.address_components[k].types;
      if(typeData[0]==='postal_code')
        this.addressFields.postalCode = results.address_components[k].long_name
      if(typeData[0]==='country'){
          this.addressFields.countryShortName = results.address_components[k].short_name;
          this.addressFields.countryLongName = results.address_components[k].long_name;
      }
      if(typeData[0]==='administrative_area_level_1')
          this.addressFields.administrativeAreaLevel1 = results.address_components[k].long_name;

      if(typeData[0]==='administrative_area_level_2')
          this.addressFields.administrativeAreaLevel2 = results.address_components[k].long_name;
    }
  }

  //Get Workshop Details
  getWorkshop() {
    this.loading = true;
    this.workshopService.workshopDetails(this.id)
          .subscribe( (data: any) => {
            if (data["statusCode"] ==0 ) {
              this.loading = false;
               if(data["error"] && data["error"]["errorCode"] == 2){
                  this.toastr.error(this.translate.instant('unauthorized'));
                  return this.localAuthService.unauthorized();
               }else {
                 //redirect to back

                 this.toastr.error('Unable to fetch workshop details');
                 this.router.navigate(['/admin/workshops'])
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["workshop"]){
                this.loading = false;
                //Now set values
                this.addressFields = {formattedAddress : data["responseData"]["workshop"]["address"]};
                this.WorkshopForm.patchValue({
                   workshopData :{
                      'title'        :  data["responseData"]["workshop"]["title"],
                      'description'  :  data["responseData"]["workshop"]["description"],
                      'startDate'    :  data["responseData"]["workshop"]["startDate"],
                      'endDate'      :  data["responseData"]["workshop"]["endDate"],
                      'seats'        :  data["responseData"]["workshop"]["seats"],
                      'cost'         :  data["responseData"]["workshop"]["cost"],
                      'category'     :  data["responseData"]["workshop"]["category"],
                      'venue'        :  data["responseData"]["workshop"]["venue"],
                      'image'        :  '',
                      'address'      :  this.addressFields.formattedAddress,
                      'venuePhone'   :  data["responseData"]["workshop"]["venuePhone"],
                      'organizer'    :  data["responseData"]["workshop"]["organizer"],
                      'phone'        :  data["responseData"]["workshop"]["phone"],
                      'website'      :  data["responseData"]["workshop"]["website"],
                      // 'latitude'     :  data["responseData"]["workshop"]["latitude"],
                      // 'longitude'    :  data["responseData"]["workshop"]["longitude"],
                      'email'        :  data["responseData"]["workshop"]["email"],
                   }
                });
                this.WorkshopForm.controls.workshopData['controls'].searchControl.setValue(this.addressFields.formattedAddress); ;
                console.log(data["responseData"], "Get Response");
                try{
                    let date: any = new Date(data["responseData"]["workshop"]["startDate"]);
                      if(date=="Invalid Date"){
                          return "";
                      }
                      let currDateObj = {year: date.getFullYear(), month: date.getMonth()+ 1,  day:  date.getDate()};
                      this.WorkshopForm.controls.workshopData['controls'].startDate.setValue(currDateObj); ;
                      console.log(this.WorkshopForm);
                    }catch(ex){
                      return "";
                    }

                try{
                    let date: any = new Date(data["responseData"]["workshop"]["endDate"]);
                      if(date=="Invalid Date"){
                          return "";
                      }
                      let currDateObj = {year: date.getFullYear(), month: date.getMonth()+ 1,  day:  date.getDate()};
                      this.WorkshopForm.controls.workshopData['controls'].endDate.setValue(currDateObj); ;
                    }catch(ex){
                      return "";
                    }

                this.WorkshopForm.controls.workshopData['controls'].searchControl.setValue(data['responseData']['workshop']['address']);

                this.serverImage = data["responseData"]["workshop"]["image"] || "";

                try{
                  this.myMarker.lat = data['responseData']['workshop']['latitude'];
                  this.myMarker.lng = data['responseData']['workshop']['longitude'];
                }catch(ex){
                  this.myMarker.lat = 43.6532;
                  this.myMarker.lng = 79.3832;
                }

             }
             else{
              console.log('status not 1 nor 0');
              this.loading = false;
              this.router.navigate(['/admin/workshops'])
             }
             console.log(data);

          },
          err => {
              this.loading = false;
              this.toastr.error('Unable to fetch workshop details');
              this.router.navigate(['/admin/workshops']);
          }
        );
  }

  //Create Workshop
  createWorkshop(){    
    this.loading = true;
    this.workshopService.addWorkshop(this.addressFields, this.myMarker, this.WorkshopForm.value.workshopData, this.image)
          .subscribe( (data: any) => {
            if (data["statusCode"] ==0 ) {
                this.loading = false;
               if(data["error"] && data["error"]["errorCode"] == 2){
                  this.toastr.error(this.translate.instant('unauthorized'));
                  return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3)){
                  this.toastr.error(this.translate.instant('plsTry'));
                }else if(data["error"] && (data["error"]["errorCode"] == 5)){
                  this.toastr.error(data["error"]["errors"][0]["message"]);
                }else{
                  this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["workshop"]){
                this.formSubmitAttempt = false;
                this.loading = false;
                this.toastr.success(data["responseData"]["message"]);
                this.router.navigate(['/admin/workshops'])
             }
             else{
              this.loading = false;
              console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             console.log(data);
            this.loading = false;
          },
          err => {
              this.loading = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
  }

  //Update Workshop
  updateWorkshop(){
    console.log("Update form api hit");
    
    this.loading = true;
    this.workshopService.updateWorkshop(this.addressFields, this.myMarker, this.id, this.WorkshopForm.value.workshopData, this.image)
          .subscribe( (data: any) => {
            if (data["statusCode"] ==0 ) {
                this.loading = false;
                if(data["error"] && data["error"]["errorCode"] == 2){
                  this.toastr.error(this.translate.instant('unauthorized'));
                  return this.localAuthService.unauthorized();
                }else if(data["error"] && (data["error"]["errorCode"] == 3)){
                  this.toastr.error(this.translate.instant('plsTry'));
                }else if(data["error"] && (data["error"]["errorCode"] == 5)){
                  this.toastr.error(data["error"]["errors"][0]["message"]);
                }else{
                  this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["workshop"]){
                this.loading = false;
                this.formSubmitAttempt = false;
                this.toastr.success(data["responseData"]["message"]);
                this.router.navigate(['/admin/workshops'])
             }
             else{
              console.log('status not 1 nor 0');
              this.loading = false;
              this.toastr.error(this.translate.instant('plsTry'));
             }
             console.log(data);
             this.loading = false;
          },
          err => {
              this.loading = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
  }

}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  draggable: boolean;
  label?: string;
}
