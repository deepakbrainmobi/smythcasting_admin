import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProdnListingComponent } from './prodn-listing/prodn-listing.component';

const routes: Routes = [
  {
   path: '',
     children: [
       { path: '', component: ProdnListingComponent }
     ]
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProdnRoutingModule { }
