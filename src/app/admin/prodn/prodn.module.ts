import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProdnRoutingModule } from './prodn-routing.module';
import { ProdnComponent } from './prodn.component';
import { ProdnListingComponent } from './prodn-listing/prodn-listing.component';

@NgModule({
  imports: [
    CommonModule,
    ProdnRoutingModule
  ],
  declarations: [ProdnComponent, ProdnListingComponent]
})
export class ProdnModule { }
