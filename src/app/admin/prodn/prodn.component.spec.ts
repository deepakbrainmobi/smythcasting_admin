import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdnComponent } from './prodn.component';

describe('ProdnComponent', () => {
  let component: ProdnComponent;
  let fixture: ComponentFixture<ProdnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
