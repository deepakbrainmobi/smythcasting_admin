import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdnListingComponent } from './prodn-listing.component';

describe('ProdnListingComponent', () => {
  let component: ProdnListingComponent;
  let fixture: ComponentFixture<ProdnListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdnListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdnListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
