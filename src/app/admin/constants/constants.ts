export class Constants {

  public static limitPage: Array<number> = [50, 100, 200];

  public static ethnicity: Array<string> = ["White","Black","Hispanic","Asian","American Indian/Alaska Native","Native Hawaiian/Other Pacific Islander"];

  public static eyeColor: Array<string> = ["Black","Brown","Red","Blue"];

  public static hairColor: Array<string> = ["Black","Brown","Red","Blue"];

  public static unionStatus: Array<string> = ["ACTRA", "SAG", "UDA", "NON-UNION"];

  public static accountPurchase: Array<string> = ["REGULAR", "A LIST"]

}
