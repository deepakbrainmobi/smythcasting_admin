import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';
import {TranslateService} from '@ngx-translate/core'; // --> need to get in ts file

import { ToastrService } from 'ngx-toastr';

import { LocalJsonService } from '../../services/local-json.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  form: FormGroup;
  public formSubmitAttempt: boolean = false;
  public showSpinner: boolean = false;
  public errorMessage: any;
  public countryCodes: any = [];

  @ViewChild('f') myForm;

    constructor(
      private fb: FormBuilder,
      private router: Router,
      private adminService: AdminService,

      public translate: TranslateService,
      private toastr: ToastrService,
      private localJsonService: LocalJsonService
    ) { }

    ngOnInit() {
      this.form = this.fb.group({
        email:  ['', Validators.compose([Validators.required,
                    Validators.pattern(/(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,9}))+$/)])
                ]
      });
      // this.loadCountryCodes();
    }

    isFieldInvalid(field: string) { // {6}
      return (
        (!this.form.get(field).valid && this.form.get(field).touched) ||
        (this.form.get(field).untouched && this.formSubmitAttempt)
      );
    }

    onSubmit() {
      console.log('onSubmit');
      this.formSubmitAttempt = true;
      if (this.form.valid) {
        this.showSpinner = true;
          this.adminService.forgotPassword(this.form.value)
          .subscribe(
            (data:any) => {

              this.showSpinner = false;
              if (data["statusCode"] ==0 ) {
                 if(data["error"] && data["error"]["errorCode"] == 7){
                     this.toastr.error(data["error"]["responseMessage"]);
                 } else if (data["error"] && (data["error"]["errorCode"] == 5)){
                      this.toastr.error(data["error"]["errors"]["message"]);
                  } else {
                   this.toastr.error(this.translate.instant('plsTry'));
                 }
               }
               else if (data["statusCode"] ==1){
                      console.log('statusCode 1');
                      this.toastr.success(data["responseData"]["message"]);
                      this.router.navigate(['/production/signin']);
               }
               else{
                console.log('statusCode not 1 nor 0');
                this.toastr.error(this.translate.instant('plsTry'));
               }
              console.log(data);
          },
          err => {
              this.showSpinner = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
      }
    }
}
