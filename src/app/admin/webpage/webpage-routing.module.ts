import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WebpageComponent } from './webpage.component';

import { WebpageListingComponent } from './webpage-listing/webpage-listing.component';
import { WebpageAddComponent } from './webpage-add/webpage-add.component';
import { WebpageDetailsComponent } from './webpage-details/webpage-details.component';
const routes: Routes = [
  {
   path: '', component: WebpageComponent ,
     children: [
       { path: '', component: WebpageListingComponent },
       { path: 'add', component: WebpageAddComponent },
       { path: 'edit/:id', component: WebpageAddComponent },
       { path: ':id', component: WebpageDetailsComponent },
     ]
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebpageRoutingModule { }
