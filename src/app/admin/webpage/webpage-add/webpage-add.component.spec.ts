import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebpageAddComponent } from './webpage-add.component';

describe('WebpageListingComponent', () => {
  let component: WebpageAddComponent;
  let fixture: ComponentFixture<WebpageAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebpageAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebpageAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
