import { Component, OnInit } from '@angular/core';
import { WebpageService } from '../../services/webpage.service';
import { PagerService } from '../../../services/pager.service';
import {Constants} from '../../constants/constants';

import {TranslateService} from '@ngx-translate/core';

import { LocalAuthService } from '../../auth/local-auth.service';
import { ToastrService } from 'ngx-toastr';

import { Router, ActivatedRoute } from '@angular/router';
import {NgForm} from '@angular/forms';
import {FormGroup,FormControl,Validators,FormArray} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-webpage-add',
  templateUrl: './webpage-add.component.html',
  styleUrls: ['./webpage-add.component.css']
})
export class WebpageAddComponent implements OnInit { 

  public WebpageForm:FormGroup;;
  public showSpinner: boolean = false;
  public image: any;
  public id: any;
  public serverImage: any;
  public formSubmitAttempt: boolean = false;
  
  constructor(
    private pagerService: PagerService,
    private webpageService: WebpageService,

    private translate: TranslateService,
    private toastr: ToastrService,
    private localAuthService: LocalAuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  webpages: any[];

  ngOnInit() {
    console.log(this.router.url, 'this.router.url');
    this.initForm();
    if(this.router.url == '/admin/webpages/add' || this.router.url == '/webpages/add') {
      // this.initForm();
    }else{
      this.id = this.route.snapshot.paramMap.get('id') || '0';
      if(this.id!='0'){
        this.getWebpage();
      }else{
        this.toastr.error("No Details Found.");
      }
    }
  }


  //Initialize Form
  initForm() {
    this.WebpageForm = new FormGroup({
      'webpageData': new FormGroup({
          'title':new FormControl(null,[Validators.required,Validators.pattern('/^[a-zA-Z]*$/')]),
          'description':new FormControl(null,[Validators.required]),
          'short_desc':new FormControl(null,[Validators.required]),
          'image':new FormControl(null),
      })
    });
  }

  //Read Url For Image
  readUrl(event:any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event:any) => {
        this.serverImage = event.target.result;
      }
      // console.log(event.target.files[0]);
      this.image = event.target.files[0];
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  //Submit Event
  onSubmit() {
    console.log(this.WebpageForm);
    if(this.router.url == '/admin/webpages/add' || this.router.url == '/webpages/add') {
      this.formSubmitAttempt = true;
      this.createWebpage(this.WebpageForm.value.webpageData)
    }else{
      this.updateWebpage(this.WebpageForm.value.webpageData)  
    }
    
  }

  //Get Webpage Details
  getWebpage() {
    // this.initForm();
    this.webpageService.webpageDetails(this.id)
          .subscribe( (data: any) => {
            if (data["statusCode"] ==0 ) {
               if(data["error"] && data["error"]["errorCode"] == 2){
                  this.toastr.error(this.translate.instant('unauthorized'));
                  return this.localAuthService.unauthorized();
               }else {
                 //redirect to back

                 this.toastr.error('Unable to fetch webpage details');
                 this.router.navigate(['/admin/webpages'])
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["page"]){
                
                //Now set values
                this.WebpageForm.patchValue({
                   webpageData :{
                      'title'        :  data["responseData"]["page"]["title"],
                      'description'  :  data["responseData"]["page"]["description"],
                      'short_desc'   :  data["responseData"]["page"]["short_desc"],
                      'image'        :  '',
                   }
                });

                this.serverImage = data["responseData"]["page"]["image"] || "";

             }
             else{
              console.log('status not 1 nor 0');
              this.router.navigate(['/admin/webpages'])
             }
             console.log(data);

          },
          err => {
              this.showSpinner = false;
              this.toastr.error('Unable to fetch webpage details');
              this.router.navigate(['/admin/webpages']);
          }
        );
  }

  //Create Webpage
  createWebpage(formValue){
    this.webpageService.addWebpage(formValue, this.image)
          .subscribe( (data: any) => {
            if (data["statusCode"] ==0 ) {
               if(data["error"] && data["error"]["errorCode"] == 2){
                  this.toastr.error(this.translate.instant('unauthorized'));
                  return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3)){
                  this.toastr.error(this.translate.instant('plsTry'));
                }else if(data["error"] && (data["error"]["errorCode"] == 5)){
                  this.toastr.error(data["error"]["errors"][0]["message"]);
                }else{
                  this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["pageListing"]){
                this.toastr.success(data["responseData"]["message"]);
                this.formSubmitAttempt = false;
                this.router.navigate(['/admin/webpages'])
             }
             else{
              console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             console.log(data);

          },
          err => {
              this.showSpinner = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
  }

  //Update Webpage
  updateWebpage(formValue){
    this.webpageService.updateWebpage(this.id, formValue, this.image)
          .subscribe( (data: any) => {
            if (data["statusCode"] ==0 ) {
               if(data["error"] && data["error"]["errorCode"] == 2){
                  this.toastr.error(this.translate.instant('unauthorized'));
                  return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3)){
                  this.toastr.error(this.translate.instant('plsTry'));
                }else if(data["error"] && (data["error"]["errorCode"] == 5)){
                  this.toastr.error(data["error"]["errors"][0]["message"]);
                }else{
                  this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["pageListing"]){
                this.formSubmitAttempt = false;
                this.toastr.success(data["responseData"]["message"]);
             }
             else{
              console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             console.log(data);

          },
          err => {
              this.showSpinner = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
  }



}
