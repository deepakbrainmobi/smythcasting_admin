import { Component, OnInit } from '@angular/core';
import { WebpageService } from '../../services/webpage.service';
import { PagerService } from '../../../services/pager.service';
import {Constants} from '../../constants/constants';

import {TranslateService} from '@ngx-translate/core';

import { LocalAuthService } from '../../auth/local-auth.service';
import { ToastrService } from 'ngx-toastr';

import { Router } from '@angular/router';

@Component({
  selector: 'app-webpage-details',
  templateUrl: './webpage-details.component.html',
  styleUrls: ['./webpage-details.component.css']
})
export class WebpageDetailsComponent implements OnInit { 

  public search: string; status: number;
  public sortData: any = {};
  public limitPage:Array<number> = Constants.limitPage;
  public showSpinner: boolean = false;
  public pageNo: number;
  public totalCount: any;
  public displayedColumns = ['S.No', 'Title', 'Start Date', 'Venue', 'Organizer', 'Phone', 'Venue Phone', 'Created', 'Action'];

  constructor(
    private pagerService: PagerService,
    private WebpageService: WebpageService,

    private translate: TranslateService,
    private toastr: ToastrService,
    private localAuthService: LocalAuthService,
    private router: Router
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  webpages: any[];

  ngOnInit() {
    this.pageNo=1;
    console.log(this.pageNo, 'pageNo-1');
  }
  
}
