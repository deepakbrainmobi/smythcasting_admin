import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebpageDetailsComponent } from './webpage-details.component';

describe('WebpageListingComponent', () => {
  let component: WebpageDetailsComponent;
  let fixture: ComponentFixture<WebpageDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebpageDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebpageDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
