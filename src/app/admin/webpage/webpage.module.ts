import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';

import { WebpageRoutingModule } from './webpage-routing.module';
import { WebpageComponent } from './webpage.component';
import { WebpageListingComponent } from './webpage-listing/webpage-listing.component';
import { WebpageAddComponent } from './webpage-add/webpage-add.component';
import { WebpageDetailsComponent } from './webpage-details/webpage-details.component';

@NgModule({
  imports: [
    CommonModule,
    WebpageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [WebpageComponent, WebpageListingComponent, WebpageAddComponent,
  				 WebpageDetailsComponent]
})
export class WebpageModule { }
