import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebpageListingComponent } from './webpage-listing.component';

describe('WebpageListingComponent', () => {
  let component: WebpageListingComponent;
  let fixture: ComponentFixture<WebpageListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebpageListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebpageListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
