import { Component, OnInit } from '@angular/core';
import { WebpageService } from '../../services/webpage.service';
import { PagerService } from '../../../services/pager.service';
import {Constants} from '../../constants/constants';

import {TranslateService} from '@ngx-translate/core';

import { LocalAuthService } from '../../auth/local-auth.service';
import { ToastrService } from 'ngx-toastr';

import { Router } from '@angular/router';

@Component({
  selector: 'app-webpage-listing',
  templateUrl: './webpage-listing.component.html',
  styleUrls: ['./webpage-listing.component.css']
})
export class WebpageListingComponent implements OnInit { 

  public search: string; status: number;
  public sortData: any = {};
  public limitPage:Array<number> = Constants.limitPage;
  public showSpinner: boolean = false;
  public pageNo: number;
  public totalCount: any;
  public displayedColumns = ['S.No', 'Title', 'Slug', 'Image', 'Action'];

  constructor(
    private pagerService: PagerService,
    private WebpageService: WebpageService,

    private translate: TranslateService,
    private toastr: ToastrService,
    private localAuthService: LocalAuthService,
    private router: Router
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  webpages: any[];

  ngOnInit() {
    this.pageNo=1;
    console.log(this.pageNo, 'pageNo-1');
    this.webpageListing(this.search, this.pageNo , this.limitPage[0], {});
  }

  webpageListing(search, pageNo, limitPage, sortInfo){
    this.WebpageService.webpageListing(search, pageNo, limitPage, sortInfo)
          .subscribe( (data: any) => {
            if (data["status"] ==0 ) {
               if(data["error"] && data["error"]["errorCode"] == 2){
                    this.toastr.error(this.translate.instant('unauthorized'));
                    return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3 ||  data["error"]["errorCode"] == 5)){
                    this.toastr.error(this.translate.instant('plsTry'));
                }else{
                 this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]  && data["responseData"]["pageListing"]){
                 
                  this.webpages = data["responseData"]["pageListing"];
             }
             else{
              //console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             //console.log(data);

          },
          err => {
              this.showSpinner = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
  }

  deleteWebpage(webpage: any){
    if(confirm("Are you sure to delete "+webpage.title+"?")) {
      this.WebpageService.deleteWebpage(webpage._id)
          .subscribe( (data: any) => {
            if (data["status"] ==0 ) {
               if(data["error"] && data["error"]["errorCode"] == 2){
                    this.toastr.error(this.translate.instant('unauthorized'));
                    return this.localAuthService.unauthorized();
               }else if(data["error"] && (data["error"]["errorCode"] == 3 ||  data["error"]["errorCode"] == 5)){
                    this.toastr.error(this.translate.instant('plsTry'));
                }else{
                 this.toastr.error(this.translate.instant('plsTry'));
               }
             }
             else if (data["statusCode"] ==1 && data["responseData"]){
                  this.toastr.success(data["responseData"]["message"]);
                  this.webpageListing(this.search, 1 , this.limitPage[0], {});
             }
             else{
              //console.log('status not 1 nor 0');
              this.toastr.error(this.translate.instant('plsTry'));
             }
             //console.log(data);

          },
          err => {
              this.showSpinner = false;
              this.toastr.error(this.translate.instant('plsTry'));
          }
        );
    }
  }

}
