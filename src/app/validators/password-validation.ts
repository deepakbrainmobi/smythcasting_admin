import {AbstractControl} from '@angular/forms';

export class PasswordValidation {
    static MatchPassword(AC: AbstractControl) {
        let password = AC.get('new_password').value||""; // to get value in input tag
        let confirmPassword = AC.get('cnf_password').value||""; // to get value in input tag
        // password=password.trim();
        // confirmPassword=password.trim();
        // if(password!="" && confirmPassword!="" && password != confirmPassword) {
        if(password != confirmPassword) {
            // console.log('false');
            return AC.get('cnf_password').setErrors( {MatchPassword: true} );
        } else {
            // console.log('true');
            return null;
        }
    }

    static oldPassword(AC: AbstractControl) {
        let password = AC.get('old_password').value||""; // to get value in input tag
        let newPassword = AC.get('new_password').value||""; // to get value in input tag
        // password=password.trim();
        // newPassword=password.trim();
        // if(password!="" && newPassword!="" && password === newPassword) {
        if(password === newPassword) {
            // console.log('false');
            return AC.get('new_password').setErrors( {OldPassword: true} );
        } else {
            // console.log('true');
            return null;
        }
    }
}
