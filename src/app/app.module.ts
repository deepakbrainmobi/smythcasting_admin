import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { SafePipe } from './shared/safe.pipe';
import { CustomDatePipe } from './shared/date-pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule }   from '@angular/forms';

import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';

import { TokenInterceptor } from './auth/token.interceptor'

import { CookieService } from 'ngx-cookie-service';
import './rxjs-operators';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { ToastrModule } from 'ngx-toastr';
import { AutofocusDirective } from './shared/autofocus.directive';

import { LocalJsonService } from './services/local-json.service';
import { WorkTaxService } from './services/work-tax.service';

import { AgmCoreModule } from '@agm/core';

import { PagerService } from './services/pager.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FilterNamesPipe} from './directives/filter-names.pipe';
import { ModalModule } from 'ngx-bootstrap';
// import {OrderByPipe} from './directives/orderby.pipe';


// import { CustomPipeModule } from './shared/pipes/custom-pipe.module';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    console.log('factory');
    return new TranslateHttpLoader(http, './assets/i18n/', '.json')

}

@NgModule({
  declarations: [
    AppComponent,
    CustomDatePipe,
    SafePipe,
    AutofocusDirective,
    FilterNamesPipe,
    // OrderByPipe
 ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, 
    NoopAnimationsModule,
    HttpClientModule,
    AppRoutingModule, 
    ReactiveFormsModule,
    FormsModule,
    ModalModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBy4tBxJdnd9t7hsNf-mLbuhNdVOuCy_P4',
      libraries : ['places']
    }),
    ToastrModule.forRoot(), // ToastrModule added
    NgbModule.forRoot(),
  ],
  providers: [

        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
        },
        CookieService, LocalJsonService, WorkTaxService, PagerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
